package com.poke.service.green.lvl.down.repository;

import com.poke.service.domain.repository.PokemonRepository;
import com.poke.service.domain.repository.entity.AbilityDB;
import com.poke.service.domain.repository.entity.EffectEntryDB;
import com.poke.service.domain.repository.entity.PokemonDB;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class PokemonRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private PokemonRepository pokemonRepository;
    private final PokemonDB pokemonDB = new PokemonDB(2L, "bulbasaur", 1, true,
            "smetext", 100, 50, "someLInk",
            List.of(new AbilityDB(1L, "AbilName", Set.of(
                    new EffectEntryDB("Some effect", "some short effect")), true, List.of(
                    new PokemonDB(1L, "name", 1, true, "smetext", 100, 50,
                            "someLInk", null)))));
    @Test
    public void whenGetPokemonById() {
        PokemonDB expPokemon = pokemonDB;
        entityManager.persist(expPokemon);
        entityManager.flush();
        Optional<PokemonDB> found = pokemonRepository.findById(1L);

        assertTrue(found.isPresent());
        assertEquals(expPokemon.getName(), found.get().getName());

    }
}