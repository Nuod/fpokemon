package com.poke.service.center.red.lvl.service;

import com.poke.service.domain.entity.AbilityEntity;
import com.poke.service.domain.entity.EffectEntryEntity;
import com.poke.service.domain.entity.PokemonEntity;
import com.poke.service.client.pokeapi.dto.ability.AbilityDTO;
import com.poke.service.client.pokeapi.dto.ability.EffectEntry;
import com.poke.service.client.pokeapi.dto.ability.PokemonSlotHidden;
import com.poke.service.client.pokeapi.dto.ability.SimpleAbility;
import com.poke.service.client.pokeapi.dto.pokemon.PokemonDTO;
import com.poke.service.client.pokeapi.dto.pokemon.SimpleAbilitySlotHidden;
import com.poke.service.client.pokeapi.dto.pokemon.SimplePokemon;
import com.poke.service.domain.service.Gateway;
import com.poke.service.domain.repository.entity.AbilityDB;
import com.poke.service.domain.repository.entity.EffectEntryDB;
import com.poke.service.domain.repository.entity.PokemonDB;
import com.poke.service.domain.repository.AbilityRepository;
import com.poke.service.domain.repository.PokemonRepository;
import com.poke.service.client.pokeapi.mapper.AbilityMapper;
import com.poke.service.client.pokeapi.mapper.PokemonMapper;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class GatewayTest {
    @Mock
    private AbilityRepository abilityRepository;
    @Mock
    private PokemonRepository pokemonRepository;
    @Mock
    private PokemonMapper pokemonMapper;
    @Mock
    private AbilityMapper abilityMapper;
    @InjectMocks
    private Gateway gateway;

    private final AbilityDB abilityDB = new AbilityDB(1L, "AbilName", Set.of(
            new EffectEntryDB("Some effect", "some short effect")), true, List.of(
            new PokemonDB(1L, "name", 1, true, "smetext", 100, 50,
                    "someLInk", List.of(new AbilityDB(1L, "AbilName", Set.of(
                    new EffectEntryDB("Some effect", "some short effect")), true, null)))));

    private final PokemonDB pokemonDB = new PokemonDB(1L, "name", 1, true,
            "smetext", 100, 50, "someLInk",
            List.of(new AbilityDB(1L, "AbilName", Set.of(
                    new EffectEntryDB("Some effect", "some short effect")), true, List.of(
                    new PokemonDB(1L, "name", 1, true, "smetext", 100, 50,
                            "someLInk", null)))));

    private final PokemonEntity pokemonEntity = new PokemonEntity(1L, "name", 1, true,
            "smetext", 100, 50, "someLInk",
            List.of(
                    new AbilityEntity(1L, "AbilName", Set.of(
                            new EffectEntryEntity("Some effect", "some short effect")), true, null)));

    private final AbilityEntity abilityEntity = new AbilityEntity(1L, "AbilName", Set.of(
            new EffectEntryEntity("Some effect", "some short effect")), true, List.of(new PokemonEntity(1L, "name", 1, true,
            "smetext", 100, 50, "someLInk",
            null)));

    private final PokemonDTO pokemonDTO = new PokemonDTO(1L, "name", 1, true,
            "smetext", 100, 50, "someLInk",
            List.of(
                    new SimpleAbilitySlotHidden(new SimpleAbility(1L, "AbilName", "someurl"))
            ));

    private final AbilityDTO abilityDTO = new AbilityDTO(1L, "AbilName", List.of(
            new EffectEntry("Some effect", "some short effect")), true, List.of(
            new PokemonSlotHidden(new SimplePokemon(1L, "name", "someLink"))
    ));

    @Test
    public void getPokemonFromDb() {
        Mockito.when(pokemonRepository.findById(1L)).thenReturn(Optional.of(pokemonDB));
        PokemonDB tempPokemonDB = pokemonDB;
        Mockito.when(pokemonMapper.toPokemon(tempPokemonDB)).thenReturn(pokemonEntity);
        tempPokemonDB.getAbilities().get(0).setPokemonDB(null);
        PokemonEntity actual = gateway.getPokemon(1L);
        Assert.assertEquals(pokemonEntity, actual);
    }

    @Test
    public void getPokemonFromApi() {
        Mockito.when(pokemonRepository.findById(1L)).thenReturn(Optional.empty());
        //Mockito.when(pokeApiService.getPokemonById(1L)).thenReturn(pokemonDTO);
        Mockito.when(pokemonMapper.toPokemonWithoutAbilities(pokemonDTO)).thenReturn(pokemonDB);
        Mockito.when(abilityRepository.findById(1L)).thenReturn(Optional.of(abilityDB));
        PokemonDB tempPokemonDB = pokemonDB;
        Mockito.when(pokemonMapper.toPokemon(tempPokemonDB)).thenReturn(pokemonEntity);
        tempPokemonDB.getAbilities().get(0).setPokemonDB(null);
        PokemonEntity actual = gateway.getPokemon(1L);
        Assert.assertEquals(pokemonEntity, actual);
    }

    @Test
    public void getAbilityfromDB() {

        Mockito.when(abilityRepository.findById(1L)).thenReturn(Optional.of(abilityDB));
        AbilityDB tempAbilityDB = abilityDB;
        Mockito.when(abilityMapper.toAbilityEntity(tempAbilityDB)).thenReturn(abilityEntity);
        tempAbilityDB.getPokemonDB().get(0).setAbilities(null);
        AbilityEntity actual = gateway.getAbility(1L);
        Assert.assertEquals(abilityEntity, actual);
    }

    @Test
    public void getAbilityfromApi() {

        Mockito.when(abilityRepository.findById(1L)).thenReturn(Optional.empty());
        //Mockito.when(pokeApiService.getAbilityById(1L)).thenReturn(abilityDTO);
        Mockito.when(abilityMapper.toAbilityWithoutPokemons(abilityDTO)).thenReturn(abilityDB);
        Mockito.when(pokemonRepository.findById(1L)).thenReturn(Optional.of(pokemonDB));
        AbilityDB tempAbilityDb = abilityDB;
        Mockito.when(abilityMapper.toAbilityEntity(tempAbilityDb)).thenReturn(abilityEntity);
        tempAbilityDb.getPokemonDB().get(0).setAbilities(null);
        AbilityEntity actual = gateway.getAbility(1L);
        Assert.assertEquals(abilityEntity, actual);
    }
    @Test
    public void getException(){
        NumberFormatException exception =
                assertThrows(NumberFormatException.class, () ->
                        gateway.getPageOfPokemonByAbilityId(-1L, null));
    }
}