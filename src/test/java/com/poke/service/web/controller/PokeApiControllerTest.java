package com.poke.service.web.controller;

import com.poke.service.client.pokeapi.IPokeApiService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class PokeApiControllerTest {

    @Autowired
    private IPokeApiService pokeApiService;
    @Autowired
    private MockMvc mockMvc;

    @Test
    void getAbilityById() throws Exception {
        mockMvc.perform(get("http://localhost:8080/poke-api/v1/services/ability/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("stench")));
    }

    @Test
    void getPokemonById() throws Exception {
        mockMvc.perform(get("http://localhost:8080/poke-api/v1/services/pokemon/1/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.height", is(7)));
    }
    @Test
    void getWrongPokemon() throws Exception {
        mockMvc.perform(get("http://localhost:8080/poke-api/v1/services/pokemon/abs/"))
                .andExpect(status().isOk())
                .andExpect(content().string("Wrong input"));
    }

}