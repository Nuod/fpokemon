package com.poke.service.web.assembler;

import com.poke.service.domain.entity.PokemonEntity;
import com.poke.service.client.pokeapi.mapper.PokemonMapperResource;
import com.poke.service.web.controller.PokeApiController;
import com.poke.service.web.dto.PokemonResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Component;

import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class PokemonResourceAssembler implements ResourceAssembler<PokemonEntity, PokemonResource> {
    @Autowired
    private PokemonMapperResource pokemonMapperResource;

    @Override
    public PokemonResource toResource(PokemonEntity pokemonEntity) {
        PokemonResource resource = pokemonMapperResource.toPokemonResource(pokemonEntity);
        resource.add(getLinks(pokemonEntity));
        return resource;
    }

    private List<Link> getLinks(PokemonEntity pokemonEntity) {
        return List.of(
                ControllerLinkBuilder.linkTo(methodOn(PokeApiController.class).getPokemonById(pokemonEntity.getId().toString())).withSelfRel(),
               linkTo(methodOn(PokeApiController.class).deletePokemonById(pokemonEntity.getId().toString())).withRel("del")
        );
    }
}
