package com.poke.service.web.assembler;

import com.poke.service.domain.entity.AbilityEntity;
import com.poke.service.client.pokeapi.mapper.AbilityResourceMapper;
import com.poke.service.web.controller.PokeApiController;
import com.poke.service.web.dto.AbilityResource;
import lombok.Data;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Component;

import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
@Data
@Component
public class AbilityResourceAssembler implements ResourceAssembler<AbilityEntity, AbilityResource> {

    private final AbilityResourceMapper abilityResourceMapper;
    @Override
    public AbilityResource toResource(AbilityEntity abilityEntity) {
        AbilityResource abilityResource = abilityResourceMapper.toAbilityResource(abilityEntity);
        abilityResource.add(getLinks(abilityEntity));
        return abilityResource;
    }
    private List<Link> getLinks(AbilityEntity abilityEntity){
        return List.of(
                ControllerLinkBuilder.linkTo(methodOn(PokeApiController.class).getAbilityById(abilityEntity.getId().toString())).withSelfRel(),
                linkTo(methodOn(PokeApiController.class).deleteAbilityById(abilityEntity.getId().toString())).withRel("del")
        );
    }
}
