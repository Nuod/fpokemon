package com.poke.service.web.controller;

import com.poke.service.domain.entity.AbilityEntity;
import com.poke.service.domain.entity.PokemonEntity;
import com.poke.service.client.pokeapi.dto.ability.SimpleAbility;
import com.poke.service.client.pokeapi.dto.pokemon.SimplePokemon;
import com.poke.service.domain.service.Gateway;
import com.poke.service.client.pokeapi.ServiceEnum;
import com.poke.service.web.assembler.AbilityResourceAssembler;
import com.poke.service.web.assembler.PokemonResourceAssembler;
import com.poke.service.web.dto.AbilityResource;
import com.poke.service.web.dto.PokemonResource;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.EnumMap;
import java.util.List;
@CrossOrigin
@RestController
@Api(value = "/poke-api/v1", produces = MediaTypes.HAL_JSON_VALUE)
@RequestMapping(value = "/poke-api/v1/services")
@RequiredArgsConstructor
public class PokeApiController {

    private final Gateway gateway;
    private final PokemonResourceAssembler pokemonResourceAssembler;
    private final AbilityResourceAssembler abilityResourceAssembler;

    @GetMapping
    public EnumMap<ServiceEnum, String> getServices() {
        return gateway.getServiceFromApi();
    }

    @GetMapping("/ability")
    public List<SimpleAbility> getSimpleAbilities() {
        return gateway.getAbilityList();
    }

    @GetMapping("/pokemon")
    public List<SimplePokemon> getSimplePokemons() {
        return gateway.getPokemonList();
    }

    @GetMapping("/pokemon/{pokemonId}")
    public PokemonResource getPokemonById(@PathVariable String pokemonId) {
        PokemonEntity pokemonEntity = gateway.getPokemon(Long.valueOf(pokemonId));
        return pokemonResourceAssembler.toResource(pokemonEntity);
    }

    @GetMapping("/ability/{abilityId}")
    public AbilityResource getAbilityById(@PathVariable String abilityId) {
        AbilityEntity abilityEntity = gateway.getAbility(Long.valueOf(abilityId));
        return abilityResourceAssembler.toResource(abilityEntity);
    }

    @DeleteMapping("/pokemon/delete/{pokemonId}")
    @ResponseStatus(code = HttpStatus.OK)
    public List<SimplePokemon> deletePokemonById(@PathVariable String pokemonId) {
        gateway.deletePokemonFromDb(Long.valueOf(pokemonId));
        return getSimplePokemons();
    }

    @DeleteMapping("/ability/delete/{abilityId}")
    @ResponseStatus(code = HttpStatus.OK)
    public List<SimpleAbility> deleteAbilityById(@PathVariable String abilityId) {
        gateway.deleteAbilityFromDb(Long.valueOf(abilityId));
        return getSimpleAbilities();
    }

}
