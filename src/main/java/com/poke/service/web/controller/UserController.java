package com.poke.service.web.controller;

import com.poke.service.domain.entity.PokemonEntity;
import com.poke.service.domain.repository.AbilityRepository;
import com.poke.service.domain.repository.ICustomAbilityRepository;
import com.poke.service.domain.repository.ICustomPokemonRepository;
import com.poke.service.domain.repository.PokemonRepository;
import com.poke.service.domain.repository.UserRepository;
import com.poke.service.domain.service.Gateway;
import com.poke.service.client.pokeapi.IPokeApiService;
import com.poke.service.domain.repository.entity.UserDb;
import com.poke.service.config.enums.UserRole;
import com.poke.service.client.pokeapi.mapper.AbilityMapper;
import com.poke.service.client.pokeapi.mapper.PokemonMapperResource;
import com.poke.service.web.assembler.PokemonResourceAssembler;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collections;

//todo Fucking legacy code, use for testing
@Data
@RestController
public class UserController {
    private final IPokeApiService pokeApiService;
    private final UserRepository userRepository;
    private final Gateway gateway;
    private final AbilityRepository abilityRepository;
    private final PokemonRepository pokemonRepository;
    private final AbilityMapper abilityMapper;
    private final PokemonMapperResource pokemonMapperResource;
    private final PokemonResourceAssembler pokemonResourceAssembler;
    private final ICustomPokemonRepository customPokemonRepository;
    private final ICustomAbilityRepository customAbilityRepository;

    @GetMapping("/test")
    public ResponseEntity start(@PageableDefault(sort = {"name"}, size = 3,
            direction = Sort.Direction.DESC) Pageable pageable,
                                PagedResourcesAssembler assembler) {
        Page<PokemonEntity> pokemonEntities = gateway.getPageOfPokemonByAbilityId(1L, pageable);
        PagedResources result = assembler.toResource(pokemonEntities, pokemonResourceAssembler);
        return ResponseEntity.ok(result);

    }

    @PostMapping("/test")
    public ResponseEntity testDB() {
        customPokemonRepository.getPokemonByName("bulbasaur");
        return ResponseEntity.ok("123");
    }

    @PostMapping("/testApi")
    public ModelAndView testApi(String id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("main");
        PokemonEntity poke = gateway.getPokemon(Long.valueOf(id));
//        PokemonDTO poke = pokeApiService.getPokemonById();
        modelAndView.addObject("pokemon", poke);
        return modelAndView;
    }

    @RequestMapping("/")
    public ModelAndView sayHi() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("greeting");
        return modelAndView;
    }

    @GetMapping("/registration")
    public String registration() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("reg");
        return "reg";
    }

    @PostMapping("/registration")
    public ModelAndView addUser(UserDb user) {
        ModelAndView modelAndView = new ModelAndView();

        UserDb userFromDb = userRepository.findByUsername(user.getUsername());
        if (userFromDb != null) {
            modelAndView.setViewName("reg");
            modelAndView.addObject("err", "User exists");
            return modelAndView;
        }
        modelAndView.setViewName("redirect:/login");
        user.setActive(true);
        user.setRoles(Collections.singleton(UserRole.USER));
        userRepository.save(user);
        return modelAndView;
    }
}
