package com.poke.service.web.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@ControllerAdvice
public class ExceptionHandlerController {
    @ResponseBody
    @ExceptionHandler(Exception.class)
    public String handleRuntimeException(Exception ex){
      log.error(ex.getMessage());
        return "Wrong input";
    }
}
