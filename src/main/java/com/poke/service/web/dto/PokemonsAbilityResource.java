package com.poke.service.web.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.poke.service.domain.entity.PokemonEntity;
import com.poke.service.client.pokeapi.dto.ability.EffectEntry;
import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

import java.util.List;

@Data
public class PokemonsAbilityResource extends ResourceSupport {
    @JsonProperty("id")
    private Long identifier;
    private String name;
    private boolean mainSeries;
    @JsonProperty("effect_entries")
    private List<EffectEntry> effectEntries;
    @JsonIgnore
    private List<PokemonEntity> pokemonDB;
}
