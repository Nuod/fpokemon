package com.poke.service.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.ResourceSupport;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PokemonResource extends ResourceSupport {
    @JsonProperty("id")
    private Long identifier;
    private String name;
    private Integer order;
    private boolean defaultPoke;
    private String baseExp;
    private Integer height;
    private Integer weight;
    private String locAreaEncounter;
    private List<PokemonsAbilityResource> abilities;
}
