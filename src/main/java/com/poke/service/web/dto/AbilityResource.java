package com.poke.service.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.poke.service.client.pokeapi.dto.ability.EffectEntry;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.ResourceSupport;

import java.util.List;
@Data
@NoArgsConstructor
public class AbilityResource extends ResourceSupport {
    @JsonProperty("id")
    private Long identifier;
    private String name;
    private boolean mainSeries;
    @JsonProperty("effect_entries")
    private List<EffectEntry> effectEntries;
    private List<AbilitysPokemonResource> pokemonDB;
}
