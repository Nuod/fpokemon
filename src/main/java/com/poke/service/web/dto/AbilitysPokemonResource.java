package com.poke.service.web.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.poke.service.domain.entity.AbilityEntity;
import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

import java.util.List;

@Data
public class AbilitysPokemonResource extends ResourceSupport {
    @JsonProperty("id")
    private Long identifier;
    private String name;
    private Integer order;
    private boolean defaultPoke;
    private String baseExp;
    private Integer height;
    private Integer weight;
    private String locAreaEncounter;
    @JsonIgnore
    private List<AbilityEntity> abilities;
}
