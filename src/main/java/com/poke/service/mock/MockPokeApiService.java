//package com.poke.service.mock;
//
//import com.poke.service.client.pokeapi.dto.ability.AbilityDTO;
//import com.poke.service.client.pokeapi.dto.ability.EffectEntry;
//import com.poke.service.client.pokeapi.dto.ability.PokemonSlotHidden;
//import com.poke.service.client.pokeapi.dto.ability.SimpleAbility;
//import com.poke.service.client.pokeapi.dto.pokemon.PokemonDTO;
//import com.poke.service.client.pokeapi.dto.pokemon.SimpleAbilitySlotHidden;
//import com.poke.service.client.pokeapi.dto.pokemon.SimplePokemon;
//import com.poke.service.client.pokeapi.IPokeApiService;
//import com.poke.service.client.pokeapi.ServiceEnum;
//import lombok.extern.slf4j.Slf4j;
//
//import java.util.EnumMap;
//import java.util.List;
//@Slf4j
//public class MockPokeApiService implements IPokeApiService {
//
//    @Override
//    public PokemonDTO getPokemonById(Long id) {
//        log.warn("Use PokeApiMock");
//        return new PokemonDTO(1L, "name", 1, true,
//                "smetext", 100, 50, "someLInk",
//                List.of(
//                        new SimpleAbilitySlotHidden(new SimpleAbility(1L, "AbilName", "someurl"))
//                ));
//    }
//
//    @Override
//    public EnumMap<ServiceEnum, String> getApiService() {
//        log.warn("Use PokeApiMock");
//        return null;
//    }
//
//    @Override
//    public AbilityDTO getAbilityById(Long id) {
//        log.warn("Use PokeApiMock");
//        return  new AbilityDTO(1L, "AbilName", List.of(
//                new EffectEntry("Some effect", "some short effect")), true, List.of(
//                new PokemonSlotHidden(new SimplePokemon(1L, "name", "someLink"))
//        ));
//    }
//
//    @Override
//    public List<SimpleAbility> getListSimpleAbility() {
//        log.warn("Use PokeApiMock");
//        return List.of(
//                new SimpleAbility(1L, "stench",
//                        "https://pokeapi.co/api/v2/ability/1/")
//        );
//    }
//
//    @Override
//    public List<SimplePokemon> getListSimplePokemon() {
//        log.warn("Use PokeApiMock");
//        return List.of(
//                new SimplePokemon(1L,"bulbasaur",
//                        "https://pokeapi.co/api/v2/pokemon/1/")
//        );
//    }
//}
