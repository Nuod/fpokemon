package com.poke.service.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.StringVendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@EnableSwagger2
@Configuration
public class SwaggerConfig {
    @Bean
    public Docket swagger() {
        Contact contact = new Contact("OOO lol","https://google.com","vasaPupkin@lol.com");
        ApiInfo apiInfo = new ApiInfo("PokeApi",
                "Proxy RESTfull NOT microservice pokemon",
                "0.0.000000001",
                null,
                contact,
                null,
                null,
                Collections.singletonList((new StringVendorExtension("name", "value"))));
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.poke.service"))
                .paths(PathSelectors.any())
                .build()
                .pathMapping("/")
                .apiInfo(apiInfo);
    }
}
