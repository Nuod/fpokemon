package com.poke.service.config;

import com.poke.service.client.pokeapi.IPokeApiService;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(clients = {
        IPokeApiService.class
})
public class FeignConfig {
}
