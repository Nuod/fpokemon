package com.poke.service.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class BeanConfig {
    @Value("${PokeApiMock.enable:false}")
    private boolean isPokeApiMock;

//    @Bean
//    public IPokeApiService getPokeApiService() {
//        if (!isPokeApiMock) {
//            return new PokeApiService();
//        } else {
//            log.warn("Use PokeApiMock");
//            return new MockPokeApiService();
//        }
//    }
}
