package com.poke.service.config;

import com.poke.service.annotation.UseMock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
@Slf4j
public class MockBeanFactoryPostprocessor implements BeanFactoryPostProcessor {
    @Value("${use.mock:false}")

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
        BeanDefinition beanDefinitionTmp = configurableListableBeanFactory.getBeanDefinition("useMock");
        Object useMocking = beanDefinitionTmp.getAttribute("use.mock");
        if (useMocking == null) {
            return;
        }
        if (!useMocking.equals("true")) {

            return;
        } else {
            final String[] beanDefNames = configurableListableBeanFactory.getBeanDefinitionNames();
            for (String beanName : beanDefNames) {

                final BeanDefinition beanDefinition = configurableListableBeanFactory.getBeanDefinition(beanName);
                Class<?> beanClass = null;
                try {
                    beanClass = Class.forName(beanDefinition.getBeanClassName());
                    String name = beanClass.getAnnotation(UseMock.class).mockClass().getName();
                    beanDefinition.setBeanClassName(name);

                } catch (Exception ignored) {}
            }
        }
    }

    @Bean
    public static BeanFactoryPostProcessor useMock(Environment environment, ConfigurableListableBeanFactory beanFactory) {
        String result = environment.getProperty("use.mock");
        BeanDefinition beanDefinition = beanFactory.getBeanDefinition("useMock");
        beanDefinition.setAttribute("use.mock", result);
        return anyWord -> new String();
    }

}
