package com.poke.service.config.enums;

public enum UserRole {
    USER,
    ADMIN
}
