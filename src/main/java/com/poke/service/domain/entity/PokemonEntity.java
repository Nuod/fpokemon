package com.poke.service.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PokemonEntity {
    private Long id;
    private String name;
    private Integer order;
    private boolean defaultPoke;
    private String baseExp;
    private Integer height;
    private Integer weight;
    private String locAreaEncounter;
    private List<AbilityEntity> abilities;

}
