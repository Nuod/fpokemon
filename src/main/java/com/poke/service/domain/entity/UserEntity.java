package com.poke.service.domain.entity;

import com.poke.service.config.enums.UserRole;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Set;

@Data
@AllArgsConstructor
public class UserEntity {
    private Long id;
    private String username;
    private String password;
    private boolean active;
    private Set<UserRole> roles;
}
