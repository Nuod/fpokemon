package com.poke.service.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AbilityEntity {
    private Long id;
    private String name;
    private Set<EffectEntryEntity> effectEntries;
    private boolean mainSeries;
    private List<PokemonEntity> pokemonDB;
}
