package com.poke.service.domain.service;

import com.poke.service.domain.entity.AbilityEntity;
import com.poke.service.domain.entity.PokemonEntity;
import com.poke.service.client.pokeapi.dto.ability.AbilityDTO;
import com.poke.service.client.pokeapi.dto.ability.SimpleAbility;
import com.poke.service.client.pokeapi.dto.pokemon.PokemonDTO;
import com.poke.service.client.pokeapi.dto.pokemon.SimplePokemon;
import com.poke.service.client.pokeapi.IPokeApiService;
import com.poke.service.client.pokeapi.ServiceEnum;
import com.poke.service.domain.repository.entity.AbilityDB;
import com.poke.service.domain.repository.entity.PokemonDB;
import com.poke.service.domain.repository.AbilityRepository;
import com.poke.service.domain.repository.PokemonRepository;
import com.poke.service.client.pokeapi.mapper.AbilityMapper;
import com.poke.service.client.pokeapi.mapper.PokemonMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.EnumMap;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Slf4j
@Service
public class Gateway {

    private final IPokeApiService pokeApiService;
    private final AbilityRepository abilityRepository;
    private final PokemonRepository pokemonRepository;
    private final PokemonMapper pokemonMapper;
    private final AbilityMapper abilityMapper;
    private final ConvertLinks convertLinks;

    //get Ability from DB, else get from API and save to DB
    public PokemonEntity getPokemon(Long id) {
        PokemonDB pokemonDB = pokemonRepository.findById(id).orElse(null);
        if (pokemonDB == null) {
            PokemonDTO pokemonDTO = pokeApiService.getPokemonById(id);

            pokemonDB = pokemonMapper.toPokemonWithoutAbilities(pokemonDTO);
            List<AbilityDB> abilities = pokemonDTO.getAbilities().stream().
                    map(o -> getAbilityWithoutPokemon(o.getAbility().getId())).
                    peek(abilityRepository::save).
                    collect(Collectors.toList());

            pokemonDB.setAbilities(abilities);
            pokemonRepository.save(pokemonDB);
        } else {

            List<AbilityDB> abilityDBS = pokemonDB.getAbilities().
                    stream().peek(o -> o.setPokemonDB(null)).
                    collect(Collectors.toList());
            pokemonDB.setAbilities(abilityDBS);

        }

        return pokemonMapper.toPokemon(pokemonDB);
    }

    //equals up
    public AbilityEntity getAbility(Long id) {
        AbilityDB abilityDB = abilityRepository.findById(id).orElse(null);
        if (abilityDB == null) {
            AbilityDTO abilityDTO = pokeApiService.getAbilityById(id);

            abilityDB = abilityMapper.toAbilityWithoutPokemons(abilityDTO);
            List<PokemonDB> pokemonDBS = abilityDTO.getPokemon().stream().
                    map(o -> getPokemonWithoutAbility(o.getPokemon().getId())).
                    peek(pokemonRepository::save).
                    collect(Collectors.toList());

            abilityDB.setPokemonDB(pokemonDBS);
            abilityRepository.save(abilityDB);
        } else {

            List<PokemonDB> pokemonDBS = abilityDB.getPokemonDB().
                    stream().peek(o -> o.setAbilities(null)).
                    collect(Collectors.toList());

            abilityDB.setPokemonDB(pokemonDBS);

        }
        return abilityMapper.toAbilityEntity(abilityDB);
    }

    public Page<PokemonEntity> getPageOfPokemonByAbilityId(Long id, Pageable pageable) {
        if (id<=0){
            throw new NumberFormatException("id<=0");
        }
        Page<PokemonDB> pokemonDbPage = pokemonRepository.findByAbilitiesId(id, pageable);
        pokemonDbPage.forEach(o -> o.setAbilities(null));
        Page<PokemonEntity> result = pokemonDbPage.map(pokemonMapper::toPokemon);
        return result;
    }

    public void deletePokemonFromDb(Long pokemonId) {
        pokemonRepository.deleteById(pokemonId);
    }

    public void deleteAbilityFromDb(Long abilityId) {
        abilityRepository.deleteById(abilityId);
    }

    @Cacheable(value = "simple_entity_cache")
    public List<SimplePokemon> getPokemonList() {
        return convertLinks.convertSimplePokemons(pokeApiService.getListSimplePokemon(0L, 100L));
    }

    @Cacheable(value = "simple_entity_cache")
    public List<SimpleAbility> getAbilityList() {
        return convertLinks.convertSimpleAbilities(pokeApiService.getListSimpleAbility(0L, 100L));
    }

    @Cacheable(value = "simple_entity_cache")
    public EnumMap<ServiceEnum, String> getServiceFromApi() {
        return convertLinks.convertServices(pokeApiService.getApiService());
    }

    private AbilityDB getAbilityWithoutPokemon(Long id) {
        AbilityDB abilityDB = abilityRepository.findById(id).orElse(null);
        if (abilityDB == null) {
            AbilityDTO abilityDTO = pokeApiService.getAbilityById(id);
            return abilityMapper.toAbilityWithoutPokemons(abilityDTO);
        } else {
            abilityDB.setPokemonDB(null);

        }
        return abilityDB;
    }

    private PokemonDB getPokemonWithoutAbility(Long id) {
        PokemonDB pokemonDB = pokemonRepository.findById(id).orElse(null);
        if (pokemonDB == null) {
            PokemonDTO pokemonDTO = pokeApiService.getPokemonById(id);
            return pokemonMapper.toPokemonWithoutAbilities(pokemonDTO);
        } else {
            pokemonDB.setAbilities(null);
        }
        return pokemonDB;
    }

    @Scheduled(fixedDelayString = "${cache.lifetime:100000}")
    @CacheEvict(cacheNames = "simple_entity_cache", allEntries = true)
    public void evictCache() {
        log.info("Clear cache");
    }


}