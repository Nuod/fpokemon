package com.poke.service.domain.service;

import com.poke.service.client.pokeapi.dto.ability.SimpleAbility;
import com.poke.service.client.pokeapi.dto.pokemon.SimplePokemon;
import com.poke.service.client.pokeapi.ServiceEnum;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ConvertLinks {

    @Value("${poke.api.url}")
    private String API_URL;

    @Value("${local.api.url}")
    private String MY_API_URL;

    public EnumMap<ServiceEnum, String> convertServices(EnumMap<ServiceEnum, String> source) {
        EnumMap<ServiceEnum, String> result = new EnumMap<ServiceEnum, String>(ServiceEnum.class);
        for (ServiceEnum key : source.keySet()) {
            String s = source.get(key);
            s = s.replaceAll(API_URL, MY_API_URL);
            result.put(key, s);
        }
        return result;
    }

    public List<SimplePokemon> convertSimplePokemons(List<SimplePokemon> simplePokemonList) {
        List<SimplePokemon> result = new ArrayList<>();
        for (SimplePokemon pokemon : simplePokemonList) {
            SimplePokemon poke = pokemon;
            poke.setUrl(pokemon.getUrl().replaceAll(API_URL, MY_API_URL));
            result.add(poke);
        }
        return result;
    }

    public List<SimpleAbility> convertSimpleAbilities(List<SimpleAbility> simpleAbilityList) {
        List<SimpleAbility> result = new ArrayList<>();
        for (SimpleAbility ability : simpleAbilityList) {
            SimpleAbility abil = ability;
            abil.setUrl(ability.getUrl().replaceAll(API_URL, MY_API_URL));
            result.add(abil);
        }
        return result;
    }
}
