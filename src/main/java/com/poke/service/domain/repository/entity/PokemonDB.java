package com.poke.service.domain.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "pokemon")
public class PokemonDB {
    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String name;
    @Column(name = "ordr")
    private Integer order;
    private boolean defaultPoke;
    private String baseExp;
    private Integer height;
    private Integer weight;
    private String locAreaEncounter;
    @ManyToMany()
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinTable(name = "pokemon_ability",
            joinColumns = @JoinColumn(name = "pokemon_id"),
            inverseJoinColumns = @JoinColumn(name = "ability_id"))
    private List<AbilityDB> abilities;
//    private List<Form> forms;
//    private List<GameIndice> gameIndices;
//    private List<PokeItem> heldItem;
//    private List<PokeMove> moves;
//    private Specie species;
//    private EnumMap<SpriteEnum, String> sprites;
//    private List<PokeStat> stats;
//    private List<TypeWithSlot> types;

}
