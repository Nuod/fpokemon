package com.poke.service.domain.repository;

import com.poke.service.domain.repository.entity.UserDb;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserDb, Long> {
    UserDb findByUsername(String username);
}
