package com.poke.service.domain.repository;

import com.poke.service.domain.repository.entity.PokemonDB;

public interface ICustomPokemonRepository {

    PokemonDB getPokemonByName (String name);

}
