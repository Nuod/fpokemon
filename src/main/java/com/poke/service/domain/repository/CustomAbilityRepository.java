package com.poke.service.domain.repository;

import com.poke.service.domain.repository.entity.AbilityDB;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
@Repository
public class CustomAbilityRepository implements ICustomAbilityRepository {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public AbilityDB getAbilityByName(String name) {
        Query query = initQuery(name);
        return (AbilityDB) query.getSingleResult();
    }

    private Query initQuery(String name) {
        Query query = entityManager.createNativeQuery("select a.* from ability as a " +
                "where name=?", AbilityDB.class);
        query.setParameter(1, name);
        return query;
    }
}
