package com.poke.service.domain.repository;

import com.poke.service.domain.repository.entity.AbilityDB;

public interface ICustomAbilityRepository {
    AbilityDB getAbilityByName(String name);
}
