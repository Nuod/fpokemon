package com.poke.service.domain.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.List;
import java.util.Set;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ability")
public class AbilityDB {
    @Id
    private Long id;
    private String name;
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "effect_entry", joinColumns = @JoinColumn(name = "ability_id"))
    private Set<EffectEntryDB> effectEntries;
    private boolean mainSeries;
    @ManyToMany
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinTable(name = "pokemon_ability",
            joinColumns = @JoinColumn(name = "ability_id"),
            inverseJoinColumns = @JoinColumn(name = "pokemon_id"))
    private List<PokemonDB> pokemonDB;

}
