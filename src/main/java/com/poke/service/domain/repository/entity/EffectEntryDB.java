package com.poke.service.domain.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "effect_entry")
public class EffectEntryDB {
    private String effect;
    @Column(name = "short_effect")
    private String shortEffect;
}
