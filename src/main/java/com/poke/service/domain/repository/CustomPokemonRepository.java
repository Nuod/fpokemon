package com.poke.service.domain.repository;

import com.poke.service.domain.repository.entity.PokemonDB;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
@RequiredArgsConstructor
@Repository
public class CustomPokemonRepository implements ICustomPokemonRepository {

    @PersistenceContext
    EntityManager entityManager;

    private final ICustomAbilityRepository customAbilityRepository;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public PokemonDB getPokemonByName(String name) {
        Query query = initQuery(name);
        customAbilityRepository.getAbilityByName("chlorophyll");
        return (PokemonDB) query.getSingleResult();
    }

    private Query initQuery(String name) {
        Query query = entityManager.createNativeQuery("select p.* from pokemon as p " +
                "where name=?", PokemonDB.class);
        query.setParameter(1, name);
        return query;
    }
}
