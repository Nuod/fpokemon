package com.poke.service.domain.repository;

import com.poke.service.domain.repository.entity.AbilityDB;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AbilityRepository extends JpaRepository<AbilityDB, Long> {
    Page<AbilityDB> findByPokemonDBId(Long id, Pageable pageable);
}
