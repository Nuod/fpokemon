package com.poke.service.domain.repository;

import com.poke.service.domain.repository.entity.PokemonDB;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PokemonRepository extends JpaRepository<PokemonDB, Long> {

    Page<PokemonDB> findByAbilitiesId(Long id, Pageable pageable);


}
