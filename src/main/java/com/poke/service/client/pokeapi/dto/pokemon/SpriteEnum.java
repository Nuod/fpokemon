package com.poke.service.client.pokeapi.dto.pokemon;

public enum SpriteEnum {
    back_default,
    back_female,
    back_shiny,
    back_shiny_female,
    front_default,
    front_female,
    front_shiny,
    front_shiny_female
}
