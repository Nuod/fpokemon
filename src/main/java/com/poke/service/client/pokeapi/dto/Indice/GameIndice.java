package com.poke.service.client.pokeapi.dto.Indice;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GameIndice {
    @JsonProperty("game_index")
    private Integer gameIndex;
    private SimpleVersion version;
}
