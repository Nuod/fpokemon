package com.poke.service.client.pokeapi.dto.pokemon;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.poke.service.client.pokeapi.dto.ability.SimpleAbility;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"slot","is_hidden"})
public class SimpleAbilitySlotHidden {
    private SimpleAbility ability;
}
