package com.poke.service.client.pokeapi.mapper;

import com.poke.service.domain.entity.PokemonEntity;
import com.poke.service.client.pokeapi.dto.pokemon.PokemonDTO;
import com.poke.service.domain.repository.entity.PokemonDB;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface PokemonMapper {

    PokemonDTO toPokemonDTO(PokemonDB source);

    PokemonDB toPokemonDB(PokemonDTO source);

    PokemonEntity toPokemon(PokemonDB source);

    PokemonDB toPokemonDB(PokemonEntity source);

    @Mapping(target = "abilities", ignore = true)
    PokemonDB toPokemonWithoutAbilities(PokemonDTO source);


}
