package com.poke.service.client.pokeapi.dto.stat;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PokemonStat {
    @JsonProperty("base_stat")
    private Integer baseStat;
    private Integer effort;
    private SimpleStat stat;
}
