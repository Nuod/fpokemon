package com.poke.service.client.pokeapi.dto.Indice;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.poke.service.client.pokeapi.dto.move.SimpleMoveLearnMethod;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VersionGroupDetail {
    @JsonProperty("level_learned_at")
    private Integer lvlLearnedAt;
    @JsonProperty("move_learn_method")
    private SimpleMoveLearnMethod moveLearnMethod;
    @JsonProperty("version_group")
    private SimpleVersionGroup simpleVersionGroup;
}
