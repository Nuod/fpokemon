package com.poke.service.client.pokeapi.mapper;

import com.poke.service.domain.entity.AbilityEntity;
import com.poke.service.web.dto.AbilityResource;
import com.poke.service.web.dto.PokemonsAbilityResource;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface AbilityResourceMapper {
    @Mappings({
            @Mapping(target="identifier", source="source.id"),

    })
    AbilityResource toAbilityResource(AbilityEntity source);

    PokemonsAbilityResource toPokemonsAbilityResource(AbilityEntity source);
}
