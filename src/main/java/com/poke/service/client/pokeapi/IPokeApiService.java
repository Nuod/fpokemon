package com.poke.service.client.pokeapi;

import com.poke.service.client.pokeapi.dto.ability.AbilityDTO;
import com.poke.service.client.pokeapi.dto.ability.SimpleAbility;
import com.poke.service.client.pokeapi.dto.pokemon.PokemonDTO;
import com.poke.service.client.pokeapi.dto.pokemon.SimplePokemon;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.EnumMap;
import java.util.List;

@FeignClient(name = "pokeApi", url = "${poke.api.url}")
public interface IPokeApiService {
    @GetMapping("/pokemon/{id}")
    PokemonDTO getPokemonById(@PathVariable Long id);

    @GetMapping
    EnumMap<ServiceEnum, String> getApiService();

    @GetMapping("/ability/{id}")
    AbilityDTO getAbilityById(@PathVariable Long id);

    @GetMapping("/ability/")
    List<SimpleAbility> getListSimpleAbility(@RequestParam Long offset, @RequestParam Long limit);

    @GetMapping("/pokemon/")
    List<SimplePokemon> getListSimplePokemon(@RequestParam Long offset, @RequestParam Long limit);
}
