package com.poke.service.client.pokeapi.mapper;

import com.poke.service.domain.entity.PokemonEntity;
import com.poke.service.web.dto.AbilitysPokemonResource;
import com.poke.service.web.dto.PokemonResource;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface PokemonMapperResource {
    @Mappings({
            @Mapping(target = "identifier", source = "source.id"),
    })
    PokemonResource toPokemonResource(PokemonEntity source);

    AbilitysPokemonResource toAbilitysPokemonResource(PokemonEntity source);
}
