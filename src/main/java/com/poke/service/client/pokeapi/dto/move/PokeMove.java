package com.poke.service.client.pokeapi.dto.move;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.poke.service.client.pokeapi.dto.Indice.VersionGroupDetail;
import lombok.*;

import java.util.List;


@Data
@NoArgsConstructor
public class PokeMove {
    private SimpleMove move;
    @JsonProperty("version_group_details")
    private List<VersionGroupDetail> versionGroupDetails;
}
