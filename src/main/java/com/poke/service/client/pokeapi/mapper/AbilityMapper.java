package com.poke.service.client.pokeapi.mapper;

import com.poke.service.domain.entity.AbilityEntity;
import com.poke.service.domain.repository.entity.AbilityDB;
import com.poke.service.client.pokeapi.dto.ability.AbilityDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface AbilityMapper {

    AbilityDTO toAbilityDto(AbilityDB abilityDB);

    AbilityDB toAbilityDB(AbilityDTO abilityDTO);

    @Mappings({
            @Mapping(target = "pokemonDB", ignore = true)
    })
    AbilityDB toAbilityWithoutPokemons(AbilityDTO source);

    AbilityDB toAbilityDB(AbilityEntity abilityEntity);

    AbilityEntity toAbilityEntity(AbilityDB abilityDB);
}
