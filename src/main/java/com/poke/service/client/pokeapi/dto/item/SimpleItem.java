package com.poke.service.client.pokeapi.dto.item;


import com.poke.service.client.pokeapi.dto.abstarct.ExtendId;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
public class SimpleItem extends ExtendId {
}
