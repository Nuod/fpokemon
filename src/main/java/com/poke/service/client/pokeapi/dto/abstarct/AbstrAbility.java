package com.poke.service.client.pokeapi.dto.abstarct;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public abstract class AbstrAbility {
    @JsonProperty("is_hidden")
    private Boolean hidden;
    private Integer slot;
}
