package com.poke.service.client.pokeapi.dto.ability;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"effect_changes","names", "flavor_text_entries", "generation"})
public class AbilityDTO {
    private Long id;
    private String name;
    @JsonProperty("effect_entries")
    private List<EffectEntry> effectEntries;
//    private SimpleGeneration generation;
    @JsonProperty("is_main_series")
    private boolean mainSeries;
    private List<PokemonSlotHidden> pokemon;

}
