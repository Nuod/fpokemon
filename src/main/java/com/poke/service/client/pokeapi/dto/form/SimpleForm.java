package com.poke.service.client.pokeapi.dto.form;

import com.poke.service.client.pokeapi.dto.abstarct.ExtendId;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
public class SimpleForm extends ExtendId {
}
