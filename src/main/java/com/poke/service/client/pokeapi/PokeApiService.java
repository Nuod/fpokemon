//package com.poke.service.client.pokeapi;
//
//import com.fasterxml.jackson.core.type.TypeReference;
//import com.fasterxml.jackson.databind.JsonNode;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.poke.service.annatation.UseMock;
//import com.poke.service.client.pokeapi.dto.ability.AbilityDTO;
//import com.poke.service.client.pokeapi.dto.ability.SimpleAbility;
//import com.poke.service.client.pokeapi.dto.pokemon.PokemonDTO;
//import com.poke.service.client.pokeapi.dto.pokemon.SimplePokemon;
//import com.poke.service.mock.MockPokeApiService;
//import kong.unirest.Unirest;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Value;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.EnumMap;
//import java.util.List;
//
//
//@Slf4j
////@Service
//@UseMock(mockClass = MockPokeApiService.class)
//public class PokeApiService implements IPokeApiService {
//
//    @Value("${api.url}")
//    private String apiUrl;
//
//    @Override
//    public PokemonDTO getPokemonById(Long id) {
//        ObjectMapper objectMapper = new ObjectMapper();
//        PokemonDTO jsonPokemon = null;
//        String request = Unirest.get(apiUrl + "pokemon/" + id.toString())
//                .asString()
//                .getBody();
//        try {
//            jsonPokemon = objectMapper.readValue(request, PokemonDTO.class);
//        } catch (IOException e) {
//            log.error(e.getMessage());
//        }
//        return jsonPokemon;
//    }
//
//    @Override
//    public EnumMap<ServiceEnum, String> getApiService() {
//        ObjectMapper objectMapper = new ObjectMapper();
//        EnumMap<ServiceEnum, String> serviceMap = null;
//        String request = Unirest.get(apiUrl)
//                .asString()
//                .getBody();
//        try {
//            serviceMap = objectMapper.readValue(request, new TypeReference<EnumMap<ServiceEnum, String>>() {
//            });
//        } catch (IOException e) {
//            log.error(e.getMessage());
//        }
//        return serviceMap;
//    }
//
//    @Override
//    public AbilityDTO getAbilityById(Long id) {
//        ObjectMapper objectMapper = new ObjectMapper();
//        AbilityDTO jsonAbility = null;
//        String request = Unirest.get(apiUrl + "ability/" + id.toString()).asString().getBody();
//        try {
//            jsonAbility = objectMapper.readValue(request, AbilityDTO.class);
//        } catch (IOException e) {
//            log.error(e.getMessage());
//        }
//        return jsonAbility;
//    }
//
//    @Override
//    public List<SimpleAbility> getListSimpleAbility() {
//        ObjectMapper objectMapper = new ObjectMapper();
//        List<SimpleAbility> abilityList = null;
//        String request = Unirest.get(apiUrl + "ability/").asString().getBody();
//        JsonNode node = null;
//        try {
//            node = objectMapper.readValue(request, JsonNode.class);
//            request = Unirest.get(apiUrl + "ability/?offset=0&limit=" + node.get("count").asText()).asString().getBody();
//            request = objectMapper.readValue(request, JsonNode.class).get("results").toString();
//            abilityList = objectMapper.readValue(request, new TypeReference<ArrayList<SimpleAbility>>() {
//            });
//
//        } catch (IOException e) {
//            log.error(e.getMessage());
//        }
//        return abilityList;
//    }
//
//    @Override
//    public List<SimplePokemon> getListSimplePokemon() {
//        ObjectMapper objectMapper = new ObjectMapper();
//        List<SimplePokemon> pokemonList = null;
//        String request = Unirest.get(apiUrl + "pokemon/").asString().getBody();
//        JsonNode node = null;
//        try {
//            node = objectMapper.readValue(request, JsonNode.class);
//            request = Unirest.get(apiUrl + "pokemon/?offset=0&limit=" + node.get("count").asText()).asString().getBody();
//            request = objectMapper.readValue(request, JsonNode.class).get("results").toString();
//            pokemonList = objectMapper.readValue(request, new TypeReference<ArrayList<SimplePokemon>>() {
//            });
//        } catch (IOException e) {
//            log.error(e.getMessage());
//        }
//        return pokemonList;
//    }
//}
