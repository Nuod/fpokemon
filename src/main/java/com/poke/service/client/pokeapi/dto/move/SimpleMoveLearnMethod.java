package com.poke.service.client.pokeapi.dto.move;


import com.poke.service.client.pokeapi.dto.abstarct.ExtendId;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
public class SimpleMoveLearnMethod extends ExtendId {
}
