package com.poke.service.client.pokeapi.dto.Indice;

import com.poke.service.client.pokeapi.dto.abstarct.ExtendId;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SimpleVersion extends ExtendId {
}
