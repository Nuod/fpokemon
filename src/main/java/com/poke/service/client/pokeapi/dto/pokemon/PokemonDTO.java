package com.poke.service.client.pokeapi.dto.pokemon;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"forms","game_indices","held_items","moves","species","sprites","stats","types"})
public class PokemonDTO {
    private Long id;
    private String name;
    private Integer order;
    @JsonProperty("is_default")
    private Boolean defaultPoke;
    @JsonProperty("base_experience")
    private String baseExp;
    private Integer height;
    private Integer weight;
    @JsonProperty("location_area_encounters")
    private String locAreaEncounter;
    private List<SimpleAbilitySlotHidden> abilities;
//    private List<SimpleForm> forms;
//    @JsonProperty("game_indices")
//    private List<GameIndice> gameIndices;
//    @JsonProperty("held_items")
//    private List<PokeItem> heldItem;
//    private List<PokeMove> moves;
//    private SimpleSpecie species;
//    private EnumMap<SpriteEnum, String> sprites;
//    private List<PokemonStat> stats;
//    private List<SimpleTypeWithSlot> types;
}
