package com.poke.service.client.pokeapi.dto.ability;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.poke.service.client.pokeapi.dto.abstarct.ExtendId;
import lombok.*;

@Data
@JsonIgnoreProperties({"count", "next","previous"})
public class SimpleAbility extends ExtendId {
    public SimpleAbility() {
    }

    public SimpleAbility(Long id, @NonNull String name, @NonNull String url) {
        super(id, name, url);
    }

    public SimpleAbility(@NonNull String name, @NonNull String url) {
        super(name, url);
    }
}
