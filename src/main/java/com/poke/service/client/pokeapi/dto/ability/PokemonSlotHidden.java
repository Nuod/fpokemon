package com.poke.service.client.pokeapi.dto.ability;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.poke.service.client.pokeapi.dto.abstarct.AbstrAbility;
import com.poke.service.client.pokeapi.dto.pokemon.SimplePokemon;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"is_hidden", "slot"})
@AllArgsConstructor
public class PokemonSlotHidden extends AbstrAbility {
    private SimplePokemon pokemon;
}
