package com.poke.service.client.pokeapi.dto.pokemon;

import com.poke.service.client.pokeapi.dto.abstarct.ExtendId;
import lombok.Data;
import lombok.NonNull;

@Data
public class SimplePokemon extends ExtendId {
    public SimplePokemon() {
    }

    public SimplePokemon(Long id, @NonNull String name, @NonNull String url) {
        super(id, name, url);
    }

    public SimplePokemon(@NonNull String name, @NonNull String url) {
        super(name, url);
    }
}
