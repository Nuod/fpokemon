package com.poke.service.client.pokeapi.dto.Indice;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VersionDetail {
    private Integer rarity;
    private SimpleVersion version;
}
