package com.poke.service.client.pokeapi.dto.generation;

import com.poke.service.client.pokeapi.dto.abstarct.ExtendId;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
public class SimpleGeneration extends ExtendId {
}
