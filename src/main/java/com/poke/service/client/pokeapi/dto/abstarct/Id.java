package com.poke.service.client.pokeapi.dto.abstarct;

import lombok.*;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.MappedSuperclass;

@Data
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
public abstract class Id {
   @javax.persistence.Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   private Long id;
}
