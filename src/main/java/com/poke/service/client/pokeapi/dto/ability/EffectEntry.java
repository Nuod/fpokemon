package com.poke.service.client.pokeapi.dto.ability;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"language"})
public class EffectEntry {
    private String effect;
    @JsonProperty("short_effect")
    private String shortEffect;
}
