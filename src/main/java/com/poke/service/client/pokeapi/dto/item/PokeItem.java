package com.poke.service.client.pokeapi.dto.item;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.poke.service.client.pokeapi.dto.Indice.VersionDetail;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@NoArgsConstructor
public class PokeItem {
    private SimpleItem item;
    @JsonProperty("version_details")
    private List<VersionDetail> versionDetail;
}
