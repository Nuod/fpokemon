package com.poke.service.client.pokeapi.dto.abstarct;

import lombok.*;
import lombok.extern.java.Log;

@Getter
@Setter
@NoArgsConstructor
@Log
public abstract class ExtendId extends Id {
    @NonNull
    private String name;
    @NonNull
    private String url;

    public ExtendId(Long id,  @NonNull String name, @NonNull String url) {
        super(id);
        this.name = name;
        this.url = url;
    }

    public ExtendId(@NonNull String name, @NonNull String url) {
        this.name = name;
        this.url = url;
    }

    public void setUrl(String url) {
        this.url = url;
        String[] splitUrl = url.split("/");
        try {
            this.setId(Long.valueOf(splitUrl[splitUrl.length - 1]));
        } catch (NumberFormatException e){
            log.warning("ERROR! Extract ID from URL");
        }
    }
}
