package com.poke.service.client.pokeapi.dto.type;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class SimpleTypeWithSlot {
    private Integer slot;
    private SimpleType type;
}
